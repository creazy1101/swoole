<?php
/**
 * redis 相關
 * User: xiant_000
 * Date: 2018/11/15
 * Time: 8:14
 */

return [
    'host' => '127.0.0.1',
    'port' => 6379,
    'out_time' => 120,
    'timeOut' => 50, // 超时时间
    'live_game_key' => 'live_game_key', // 直播，存放 连上来的用户 fd 前缀
];