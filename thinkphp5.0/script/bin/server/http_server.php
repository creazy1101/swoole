<?php
/**
 * Created by PhpStorm.
 * User: baidu
 * Date: 18/2/28
 * Time: 上午1:39
 */
$http = new swoole_http_server("0.0.0.0", 8811);

$http->set(
    [
        'enable_static_handler' => true,
//        'document_root' => "/home/work/swoole/data",123
        'document_root' => "/home/work/swoole/thinkphp5.0/public",
        'worker_num'    => 5,
        'content-type' => 'text/html; charset=utf-8',
    ]
);
$http->on('WorkerStart', function (swoole_server $server, $worker_id) {
    // 定义应用目录
    define('APP_PATH', __DIR__ . '/../../../application/');
    // 1. 加载基础文件
    require __DIR__ . '/../../../thinkphpcore/base.php';
    // 这里不能使用这种方式加载框架内容，不信你可以打开试试
    // require __DIR__ . '/../thinkphpcore/start.php';
});
$http->on('request', function($request, $response) use ($http) {
    //print_r($request->get);
    $content = [
        'date:' => date("Ymd H:i:s"),
        'get:' => $request->get,
        'post:' => $request->post,
        'header:' => $request->header,
    ];

    swoole_async_writefile(__DIR__."/access.log", json_encode($content).PHP_EOL, function($filename) {
        // todo
    }, FILE_APPEND);

    $_SERVER = [];
    if (isset($request->server)) {
        foreach ($request->server as $k => $v) {
            $_SERVER[strtoupper($k)] = $v;
        }
    }

    $_HEADER = [];
    if (isset($request->header)) {
        foreach ($request->header as $k => $v) {
            $_HEADER[strtoupper($k)] = $v;
        }
    }

    $_GET = [];
    if (isset($request->get)) {
        foreach ($request->get as $k => $v) {
            $_GET[$k] = $v;
        }
    }

    $_POST = [];
    if (isset($request->post)) {
        foreach ($request->post as $k => $v) {
            $_POST[$k] = $v;
        }
    }

    // 2. 执行应用
    ob_start();
    try {
        think\App::run()->send();
    } catch (Exception $e) {
        echo $e->getMessage();
    }
//    echo '-action-' . request()->action() . PHP_EOL;
    $res = ob_get_contents();
    ob_end_clean();
//    $response->cookie("singwa", "xsssss", time() + 1800);
//    $response->end("sss". json_encode($request->get));
    $response->end($res);
    // 这是种简单粗暴的销毁进程、重新加载框架内容的方式
    // $http->close($request);
});

$http->start();
