<?php
/**
 * Created by PhpStorm.
 * User: xiant_000
 * Date: 2018/11/25
 * Time: 14:11
 */

namespace app\admin\controller;


use app\common\lib\Util;

class Image
{
    public function index()
    {
        $file = request()->file('file');
        $info = $file->move('../public/upload');
        if ($info) {
            $data = [
                'image' => config('live.host') . '/upload/' . $info->getSaveName(),
            ];
            return Util::show(config('code.success'), 'ok', $data);
        } else {
            return Util::show(config('code.error'), 'error', []);
        }
    }
}