<?php
/**
 * Created by PhpStorm.
 * User: xiant_000
 * Date: 2018/11/15
 * Time: 8:25
 */

namespace \app\common\lib;

class Redis
{
    public static $pre = "sms_";
    /*
     * user key
     */
    public static $userpre = "user_";

    public static function smsKey($phone)
    {
        return self::$pre . $phone;
    }

    /**
     * 用户key
     *
     * @param $phone
     * @return string
     */
    public static function userKey($phone)
    {
        return self::$userpre.$phone;
    }
}