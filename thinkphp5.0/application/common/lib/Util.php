<?php
/**
 * Created by PhpStorm.
 * User: xiant_000
 * Date: 2018/11/15
 * Time: 8:33
 */

namespace app\common\lib;

class Util
{
    /**
     * API 输出格式
     * @param $status
     * @param string $message
     * @param array $data
     */
    public static function show($status, $message = '', $data = []) {
        $result = [
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ];

        echo json_encode($result);
    }
}