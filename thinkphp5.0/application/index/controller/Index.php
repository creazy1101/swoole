<?php

namespace app\index\controller;

use think\request;
use think\Config;

class Index
{
    public function index(Request $request)
    {
	var_dump(Config::get());
        return 'hello-swoole';
    }

    public function check()
    {
	    print_r($_GET);
        return time();
    }

    public function getClientIp()
    {
        $list = swoole_get_local_ip();
        print_r($list);
    }

    public function outputServer()
    {
        var_dump($_POST);
    }
}
