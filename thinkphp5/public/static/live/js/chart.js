// var wsUrl = "ws://121.42.24.161:8811"; // 华北1区
var wsUrl = "ws://47.101.139.0:8812"; // 华东2区

var websocket = new WebSocket(wsUrl);

//实例对象的onopen属性
websocket.onopen = function(evt) {
    websocket.send("hello-sinwa");
    console.log("8812-conected-swoole-success");
}

// 实例化 onmessage
websocket.onmessage = function(evt) {
    // evt.data
    push(evt.data);
    console.log("ws-server-return-data:" + evt.data);
}

//onclose
websocket.onclose = function(evt) {
    console.log("close");
}
//onerror

websocket.onerror = function(evt, e) {
    console.log("error:" + evt.data);
}

function push(data) {
    data = JSON.parse(data);
    html = '<div class="comment">';
    html += '<span>'+data.user+'</span>';
    html += '<span>'+data.content+'</span>';
    html += '</div>';

    $('#comments').append(html);
}