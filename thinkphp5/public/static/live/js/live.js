// var wsUrl = "ws://121.42.24.161:8811"; // 华北1区
var wsUrl = "ws://47.101.139.0:8811"; // 华东2区

var websocket = new WebSocket(wsUrl);

//实例对象的onopen属性
websocket.onopen = function(evt) {
    // websocket.send("hello-sinwa");
    console.log("conected-swoole-success");
}

// 实例化 onmessage
websocket.onmessage = function(evt) {
    // evt.data
    push(evt.data);
    console.log("ws-server-return-data:" + evt.data);
}

//onclose
websocket.onclose = function(evt) {
    console.log("close");
}
//onerror

websocket.onerror = function(evt, e) {
    console.log("error:" + evt.data);
}

function push(data) {
    data = JSON.parse(data);
    html = '<div class="frame">';
    html += '<h3 class="frame-header">';
    html +=  '<i class="icon iconfont icon-shijian"></i>李献涛第'+data.type+'节 01：30';
    html += '</h3>';
    html += '<div class="frame-item">';
    html += '<span class="frame-dot"></span>';
    html += '<div class="frame-item-author">';
            if (data.logo) {
                html += '<img src="./imgs/team1.png" width="20px" height="20px" /> 马刺';
            }
    html += data.title;
    html += '</div>';
    html += '<p>' + data.content +'</p>';
    html += '</div>';
    html += '</div>';

    $('#match-result').prepend(html);
}