<?php
/**
 * Created by PhpStorm.
 * User: xiant_000
 * Date: 2018/11/25
 * Time: 15:50
 */

namespace app\admin\controller;


use app\common\lib\redis\Predis;
use app\common\lib\Util;

/**
 * 直播推送类
 * 
 * @Category category
 * @Package  package
 * @Auther   auther
 * @License  license
 * @Link     link
 */
class Live
{
    /**
     * 直播，推送消息
     * 
     * @return json
     */
    public function push()
    {
        if (empty($_GET)) {
            return Util::show(config('code.error'), 'error');
        }
        // 校验管理员用户
        $teams = [
            1 => [
                'name'  => '马刺',
                'logo'  => 'live/imgs/team1.png'
            ],
            4 => [
                'name'  => '火箭',
                'logo'  => 'live/imgs/team4.png'
            ],
        ];
        $data = [
            'type' => intval($_GET['type']),
            'title' => !empty($teams[$_GET['team_id']]['name']) ? $teams[$_GET['team_id']]['name'] : '直播员',
            'logo' => !empty($teams[$_GET['team_id']]['logo']) ? $teams[$_GET['team_id']]['logo'] : '',
            'content' => !empty($_GET['content']) ? $_GET['content'] : '',
            'image' => !empty($_GET['image']) ? $_GET['image'] : '',
        ];
        // 获取连接的用户
        // 1 赛况信息入库 2 组装好数组 push 到直播页面
        // $clients = Predis::getInstance()->sMembers(config('redis.live_game_key'));

        $taskData = [
            'method'    => 'pushLive',
            'data'  => $data
        ];
        $_POST['http_server']->task($taskData);
        return Util::show(config('code.success'), 'success');

        // $clients = [
        //     3, 4, 5, 6, 7, 8, 9, 10
        // ];
        // foreach ($clients as $fd) {
        //     $_POST['http_server']->push($fd, json_encode($data));
        // }
    }
}
