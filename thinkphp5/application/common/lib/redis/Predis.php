<?php
/**
 * Created by PhpStorm.
 * User: xiant_000
 * Date: 2018/11/24
 * Time: 23:11
 */

namespace app\common\lib\redis;


class Predis
{
    public $redis = '';
    /**
     * 定义单利模式的变量
     * @var null
     */
    private static $_instance = null;

    public static function getInstance()
    {
        if (self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __construct()
    {
        $this->redis = new \Redis();
        $result = $this->redis->connect(
            config('redis.host'),
            config('redis.port'),
            config('redis.timeOut')
        );

        if ($result === false) {
            throw new \Exception('redis connect error');
        }

    }

    /**
     * set
     *
     * @param $key
     * @param $value
     * @param int $time
     * @return bool|string
     */
    public function set($key, $value, $time = 0)
    {
        if ($key) {
            return '';
        }
        if (is_array($value)) {
            $value = json_encode($value);
        }
        if (!$time) {
            return $this->redis->set($key, $value);
        }

        return $this->redis->setex($key, $time, $value);
    }

    public function get($key)
    {
        if ($key) {
            return '';
        }

        return $this->redis->get($key);
    }

//    /**
//     * @param $key
//     * @param $value
//     * @return int
//     */
//    public function sadd($key, $value)
//    {
//        return $this->redis->sAdd($key, $value);
//    }
//
//    /**
//     * @param $key
//     * @param $value
//     * @return int
//     */
//    public function srem($key, $value)
//    {
//        return $this->redis->sRem($key);
//    }

    /**
     * @param $key
     * @return mixed
     */
    public function sMembers($key)
    {
        return $this->sMembers($key);
    }

    /**
     * @param $name
     * @param $arguments
     */
    public function __call($name, $arguments)
    {
        if (count($arguments) != 2) {
            return '';
        }
        // TODO: Implement __call() method.
        $this->redis->$name($arguments[0], $arguments[1]);
    }
}