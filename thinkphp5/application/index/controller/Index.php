<?php

namespace app\index\controller;

use think\request;
use think\Config;
use app\common\lib\Util;

class Index
{
    public function index(Request $request)
    {
	//var_dump(Config::get());
        return 'hello-swoole' . "\r\n";
    }

    public function check()
    {
	    print_r($_GET);
        return time() . Util::show(config('code.error'), '验证码发送失败');;
    }

    public function getClientIp()
    {
        $list = swoole_get_local_ip();
        print_r($list);
    }

    public function ip()
    {
        $list = swoole_get_local_ip();
        print_r($list);
    }

    public function outputServer()
    {
        var_dump($_POST);
    }
}
