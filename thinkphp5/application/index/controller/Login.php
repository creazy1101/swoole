<?php
/**
 * Created by PhpStorm.
 * User: xiant_000
 * Date: 2018/11/24
 * Time: 23:06
 */

namespace app\index\controller;

use app\common\lib\Util;
use app\common\lib\redis\Predis;
use \app\common\lib\Redis;


class Login
{
    public function index()
    {
        var_dump($_GET);
        // 接收 phone code
        $phoneNum = intval($_GET['phone_num']);
        $code = intval($_GET['code']);

        if (empty($phoneNum) || empty($code)) {
            return Util::show(config('code.error'), 'ppone or code is error');
        }

        // redis code 只能使用同步redis
        try {
            $redisCode = Predis::getInstance()->get(Redis::smsKey($phoneNum));
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        if ($redisCode == $code) {
            // 写入 redis
            $data = [
                'user' => $phoneNum,
                'srcKey' => md5(Redis::userkey($phoneNum)),
                'time' => time(),
                'isLogin'   => true
            ];
            Predis::getInstance()->set(Redis::userkey($phoneNum), $data);

            return Util::show(config('code.success'), 'ok', $data);
        } else {
            return Util::show(config('code.error'), 'login error', []);
        }
    }
}