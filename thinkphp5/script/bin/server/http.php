<?php
/**
 * Created by PhpStorm.
 * User: xiant_000
 * Date: 2018/11/25
 * Time: 8:34
 */

use app\common\lib\Util;

class Http
{
    const HOST = '0.0.0.0';
    const PORT = 8811;

    public $http = null;
    public function __construct()
    {
        $this->http = new swoole_http_server(self::HOST, self::PORT);

        $this->http->on('workerstart', [$this, 'onWorkerStart']);
        $this->http->on('request', [$this, 'onRequest']);
        $this->http->on('close', [$this, 'onClose']);
        // task
        $this->http->on('task', [$this, 'onTask']);
        $this->http->on('finish', [$this, 'onFinish']);

        $this->http->set([
            'enable_static_handler' => true,
            'document_root' => '/home/work/swoole/thinkphp5.0/public',
            'worker_num'    => 4,
            'task_worker_num'    => 4,
        ]);

        $this->http->start();
    }

    /**
     * 监听 ws 连接事件
     *
     * @param $ws
     * @param $request
     */
    public function onWorkerStart(swoole_server $server, $worker_id)
    {
        // 定义应用目录
        define('APP_PATH', __DIR__ . '/../../../application/');
        // 1. 加载基础文件
//        require __DIR__ . '/../thinkphpcore/base.php';
        require __DIR__ . '/../../../thinkphpcore/start.php';
    }

    /**
     * 监听 http 请求事件
     *
     * @param $ws
     * @param $frame
     */
    public function onRequest($request, $response)
    {
        $content = [
            'date:' => date("Ymd H:i:s"),
            'get:' => $request->get,
            'post:' => $request->post,
            'header:' => $request->header,
        ];

        swoole_async_writefile(__DIR__."/access.log", json_encode($content).PHP_EOL, function($filename) {
            // todo
        }, FILE_APPEND);

        $_SERVER = [];
        if (isset($request->server)) {
            foreach ($request->server as $k => $v) {
                $_SERVER[strtoupper($k)] = $v;
            }
        }

        $_HEADER = [];
        if (isset($request->header)) {
            foreach ($request->header as $k => $v) {
                $_HEADER[strtoupper($k)] = $v;
            }
        }

        $_GET = [];
        if (isset($request->get)) {
            foreach ($request->get as $k => $v) {
                $_GET[$k] = $v;
            }
        }

        $_POST = [];
        if (isset($request->post)) {
            foreach ($request->post as $k => $v) {
                $_POST[$k] = $v;
            }
        }

        $_POST['http_server'] = $this->http;

        // 2. 执行应用
        ob_start();
        try {
            // think\App::run()->send();
            think\Container::get('app')->run()->send();
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        $res = ob_get_contents();
        ob_end_clean();
        $response->end($res);
    }

    /**
     * @param $serv
     * @param $taskId
     * @param $workerId
     * @param $data
     * @return string
     */
    public function onTask($serv, $taskId, $workerId, $data)
    {
        // 分发 task 任务机制，让不同的任务 走不通的逻辑
        $obj = new app\common\lib\task\Task;
        $method = $data['method'];
        $flag = $obj->$method($data['data']);
        /*
        $obj = new app\common\lib\ali\Sms();
        try {
            $response = $obj::sendSms($data['phone'], $data['code']);
        } catch (\Exception $e) {
            return Util::show('config.error', '阿里大于内部异常');
        }
        */
        return $flag; // 告诉 worker
    }

    /**
     * @param $serv
     * @param $taskId
     * @param $data
     */
    public function onFinish($serv, $taskId, $data)
    {
        echo 'taskId' . $taskId . PHP_EOL;
        echo 'finish-data-success:' . $data . PHP_EOL;
    }

    /**
     * ws 关闭连接事件
     *
     * @param $ws
     * @param $fd
     */
    public function onClose($ws, $fd)
    {
        echo 'clientId' . $fd . "\n";
    }
}

new Http();
