<?php
/**
 * Created by PhpStorm.
 * User: xiant_000
 * Date: 2018/11/25
 * Time: 8:34
 */

use app\common\lib\Util;
use app\common\lib\task\Task as TaskLib;

class Ws
{
    const HOST = '0.0.0.0';
    const PORT = 8811;
    const CHART_PORT = 8812;

    public $ws = null;
    public function __construct()
    {
        // 获取 redis 数据，若不为空，删除
        /**
         * ,,,,,,,,,,,,,,,,,,,,,,,,,,,,
         */
        $this->ws = new swoole_websocket_server(self::HOST, self::PORT);

        $this->ws->listen(self::HOST, self::CHART_PORT, SWOOLE_SOCK_TCP);

        $this->ws->on('start', [$this, 'onStart']);
        $this->ws->on('open', [$this, 'onOpen']);
        $this->ws->on('message', [$this, 'onMessage']);
        $this->ws->on('workerstart', [$this, 'onWorkerStart']);
        $this->ws->on('request', [$this, 'onRequest']);
        $this->ws->on('close', [$this, 'onClose']);
        // task
        $this->ws->on('task', [$this, 'onTask']);
        $this->ws->on('finish', [$this, 'onFinish']);

        $this->ws->set(
            [
            'enable_static_handler' => true,
            'document_root' => '/home/work/swoole/thinkphp5/public',
            'worker_num'    => 4,
            'task_worker_num'    => 4,
            ]
        );

        $this->ws->start();
    }

    /**
     * 连接建立
     * 
     * @param $server server
     */
    public function onStart($server)
    {
        swoole_set_process_name("live_master");
    }

    /**
     * 监听 ws 连接事件
     *
     * @param array $server    ws对象
     * @param int   $worker_id workder_id
     */
    public function onWorkerStart(swoole_server $server, $worker_id)
    {
        // 定义应用目录
        define('APP_PATH', __DIR__ . '/../../../application/');
        // 1. 加载基础文件
        // include __DIR__ . '/../../../thinkphpcore/base.php';
        include __DIR__ . '/../../../public/index.php';
        // include __DIR__ . '/../../../application/common/lib/Util.php';
        // Util::show('110', '你妹');
    }

    /**
     * 监听 http 请求事件
     *
     * @param $request
     * @param $response
     */
    public function onRequest($request, $response)
    {
        // 过滤掉 favicon.ico 请求
        if ($request->server['request_uri'] == '/favicon.ico') {
            $response->status(404);
            $response->end();
            return;
        }
        $content = [
            'date:' => date("Ymd H:i:s"),
            'get:' => $request->get,
            'post:' => $request->post,
            'header:' => $request->header,
        ];

        swoole_async_writefile(
            __DIR__."/access.log", 
            json_encode($content).PHP_EOL, 
            function ($filename) {
                // todo
            },
            FILE_APPEND
        );

        // $_SERVER = [];
        if (isset($request->server)) {
            foreach ($request->server as $k => $v) {
                $_SERVER[strtoupper($k)] = $v;
            }
        }

        $_HEADER = [];
        if (isset($request->header)) {
            foreach ($request->header as $k => $v) {
                $_HEADER[strtoupper($k)] = $v;
            }
        }

        $_GET = [];
        if (isset($request->get)) {
            foreach ($request->get as $k => $v) {
                $_GET[$k] = $v;
            }
        }

        $_POST = [];
        if (isset($request->post)) {
            foreach ($request->post as $k => $v) {
                $_POST[$k] = $v;
            }
        }

        $_FILES = [];
        if (isset($request->files)) {
            foreach ($request->files as $k => $v) {
                $_FILES[$k] = $v;
            }
        }

        $this->writeLog();
        $_POST['http_server'] = $this->ws;

        // 2. 执行应用
        ob_start();
        try {
            think\Container::get('app')->run()->send();
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        $res = ob_get_contents();
        if ($res) ob_end_clean();
        $response->end($res);
    }

    /**
     * 监听 ws 连接事件
     *
     * @param $ws
     * @param $request
     */
    public function onOpen($ws, $request)
    {
        // var_dump(\app\common\lib\redis\Predis::getInstance()->get("lixiantao"));
        // die;
        //     fd into redis
        // \app\common\lib\redis\Predis::getInstance()->sAdd('live_game_key', $request->fd);
    }

    /**
     * 监听 ws 消息事件
     *
     * @param $ws
     * @param $frame
     */
    public function onMessage($ws, $frame)
    {
        echo 'ser-push-message:' . $frame . "\n";
        // tod 10s
        $data = [
            'task'  => 1,
            'fd'    => $frame->fd,
        ];
//        $ws->task($data);
        //$ws->push($frame->fd, 'server-push' . date("Y-m-d H:i:s"));
    }

    /**
     * 异步任务
     * 
     * @param  $serv     serv
     * @param  $taskId   taskId
     * @param  $workerId workerId
     * @param  $data     data
     * 
     * @return string
     */
    public function onTask($serv, $taskId, $workerId, $data)
    {
        // 分发 task 任务机制，让不同的任务 走不同的逻辑
        $obj = new TaskLib;
        $method = $data['method'];
        $flag = $obj->$method($data['data'], $serv);
        /*
        $obj = new app\common\lib\ali\Sms();
        try {
            $response = $obj::sendSms($data['phone'], $data['code']);
        } catch (\Exception $e) {
            return Util::show('config.error', '阿里大于内部异常');
        }
        */
        return $flag; // 告诉 worker
    }

    /**
     * @param $serv
     * @param $taskId
     * @param $data
     */
    public function onFinish($serv, $taskId, $data)
    {
        echo 'taskId' . $taskId . PHP_EOL;
        echo 'finish-data-success:' . $data . PHP_EOL;
    }

    /**
     * ws 关闭连接事件
     *
     * @param $ws
     * @param $fd
     */
    public function onClose($ws, $fd)
    {
//        \app\common\lib\redis\Predis::getInstance()->sRem(config('redis.live_game_key'), fd);
        echo 'clientId' . $fd . "\n";
    }

    public function writeLog()
    {
        $datas = array_merge(['date' => date("Ymd H:i:s")], $_GET, $_POST, $_SERVER);

        $logs = "";
        foreach ($datas as $key => $value) {
            $logs .= $key . ":" . json_encode($value);
        }

        $dirPre = APP_PATH . '../runtime/log/' . date("Ym");
        if (!is_dir($dirPre)) {
            mkdir($dirPre, 0777, true);
        }
        $accessLogFile = $dirPre . '/' .date('d') . '_access.log';
        if (!file_exists($accessLogFile)) {
            touch($accessLogFile);
        }
        swoole_async_writefile($accessLogFile, $logs . PHP_EOL, function ($fileName) {
            // todo
        }, FILE_APPEND);
    }
}

new Ws();

//

// sigterm sigusr1 usr2
