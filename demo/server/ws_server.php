<?php
/**
 * Created by PhpStorm.
 * User: xiant_000
 * Date: 2018/11/11
 * Time: 7:40
 */
$server = new swoole_websocket_server("0.0.0.0", 8812);
$http->set(
	[
		'enable_static_handler' => true,
		'document_root' => "/home/work/swoole/data",
	]
);

// 坚挺 websocke 连接打开事件
$server->on('open', function (swoole_websocket_server $server, $request) {
    echo "server: handshake success with fd{$request->fd}\n";
});

// 监听 ws 消息事件
$server->on('message', function (swoole_websocket_server $server, $frame) {
    echo "receive from {$frame->fd}:{$frame->data},opcode:{$frame->opcode},fin:{$frame->finish}\n";
    $server->push($frame->fd, "singwa-push-success");
});

$server->on('close', function ($ser, $fd) {
    echo "client {$fd} closed\n";
});

$server->start();
