<?php
//创建Server对象，监听 127.0.0.1:9501端口
$serv = new swoole_server("0.0.0.0", 9501); 

//监听连接进入事件
// $fd 客户端连接的唯一标识
$serv->on('connect', function ($serv, $fd, $reactorId) {  
    echo "Client: $fd - $reactorId Connect.\n";
});

//监听数据接收事件
$serv->on('receive', function ($serv, $fd, $reactorId, $data) {
    $serv->send($fd, "Server $fd - $reactorId: ".$data);
});

//监听连接关闭事件
$serv->on('close', function ($serv, $fd, $reactorId) {
    echo "Client $fd - $reactorId: Close.\n";
});

//启动服务器
$serv->start(); 
